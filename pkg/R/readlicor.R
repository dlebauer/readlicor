`readlicor` <-
function(filename=file.choose(),
					  writefile=TRUE,
					  outputfile=NA,
					  average=FALSE,
					  returndata=FALSE,
					  recompute=FALSE,
					  BLCslope=-0.2196,  # from Dave
					  BLCoffset=2.7374,
					  K=1,
					  Area=NA
					  ){
	
	# Setup list of data if returning data:
	datalist <- list()
	elnr <- 1
	
	# Function for trimming white space (leading and/or trailing). Includes tabs.
	trim <- function(str)gsub('^[[:space:]]+', '', gsub('[[:space:]]+$', '', str))

	# Function for converting all columns in a dataframe to numeric
	numericdfr <- function(dfr){
					for(i in 1:ncol(dfr)){
						options(warn=-1)
						oldna <- sum(is.na(dfr[,i]))
						num <- as.numeric(as.character(dfr[,i]))
						if(sum(is.na(num)) > oldna)
							next
						else
							dfr[,i] <- num
					}
				options(warn=0)
	return(dfr)
	}
	
	# make outputfilename
	if(is.na(outputfile)){
		s <- strsplit(filename, "//")[[1]]
		# Append .csv to the output file, unless file already ends in .csv:
		if(length(grep(".csv$", tolower(s))) == 0){
			outputfile <- paste(s[length(s)], ".csv", sep="")
		} else {
			s <- s[length(s)]
			outputfile <- paste(strsplit(s, ".csv")[[1]], "_converted.csv")
		}
	}
	
	r <- readLines(filename)

	# Get rid of "Stability=" lines, they are sometimes mistaken for a comment:
	stablines <- grep("Stability=", r, fixed=TRUE)
	stablines <- c(stablines, grep("Stability:", r, fixed=TRUE)) # some versions of OPEN...
	if(length(stablines))r <- r[-stablines]
	
	# Labels (they are a bit different for versions of OPEN).
	labelline <- grep("$STARTOFDATA$",r,fixed=TRUE)[1] + 1
	
	if(is.na(labelline)){
		stop("You are trying to convert a very old Li-6400 file - insert $STARTOFDATA$ lines manually in the file.")
	}
	labelnames <- strsplit(r[labelline],",")[[1]]
	
	# Tab-delimited:
	if(length(labelnames)==1)labelnames <- strsplit(r[labelline],"\t")[[1]]
	
	# Get rid of "\""
	labelnames <- gsub("\"", "", labelnames)
	
	# Find where the fluorescence variables are, if any:
	fluorstart <- grep("^Fo$", labelnames)
	fluorend <- grep("^PhiPS2$", labelnames)

	# Potentially goes up to ETR (version difference?)
	fluorend_etr <- grep("^ETR$", labelnames)
	if(length(fluorend_etr)>0 && fluorend_etr > fluorend)fluorend <- fluorend_etr
	
	if(length(fluorstart)>0)
		fluorcols <- fluorstart:fluorend
	else
		fluorcols <- NA


	# And the comment, the date, and the unit name.
	varnames <- c("Date", "Unit", "OPENversion", "Comment",labelnames)
	
	# Add range of some variables (see below)
	varnames <- c(varnames, "PhotRange", "CondRange", "CO2RRange","Nlogs")
	
	# write labels to file.
	if(writefile)write(varnames, outputfile, append=FALSE, ncolumns=length(varnames), sep=",")
	
	# Find the name of the Unit, for saving in the output files.
	UnitName <- strsplit(r[grep("Unit=",r)[1]], ",")[[1]][2]	# comma delimited
	if(is.na(UnitName))UnitName <- strsplit(r[grep("Unit=",r)[1]], "\t")[[1]][2]	# tab delimited
	if(is.na(UnitName))UnitName <- "unknown"	# unknown (some OPEN versions don't write unit name).
	
	# Find data blocks
	datastarts <- intersect(grep("Obs",r), grep("Photo",r))
	logfilestarts <- grep("OPEN ",r)
	
	# Find version of OPEN (line ends in "\"", if not then try again!).
	OPENversion <- strsplit(strsplit(trim(r[logfilestarts[1]]), "OPEN ")[[1]][2], "\"")[[1]]  
	#"
	if(all(is.na(OPENversion)))OPENversion <- strsplit(r[logfilestarts[1]], "OPEN ")[[1]][2]
	OPENversion <- trim(OPENversion)

	# Append EOF to logfilestarts
	logfilestarts <- c(logfilestarts, length(r)+1)
	
	# Function for averaging: take the first argument when data are not numeric,
	# and the mean if they are.
	safemean <- function(x){
			options(warn= -1)
			nc <- function(st)as.numeric(as.character(st))
			p <- ifelse(all(is.na(nc(x))), x[1], mean(nc(x)))
			options(warn=1)
			p
	}
	
	# Function for getting decimal time of day from a strptime type time.
	dectime_fun <- function(TIME){
		TIME$hour / 24 + TIME$min / (24*60) + TIME$sec / (24*60*60)
	}

	for(i in 1:length(datastarts)){
	
		# Log file starts just after data series ends (hence the -1)
		# Also omit the line with labels (assumed this is always the same), hence the +1
		chunk <- (datastarts[i]+1):(logfilestarts[i+1]-1)
		logdata <- r[chunk]
		
		# Nasty fix: in fluorescence datasets, the last block is not used but avoids counter 
		# from reaching EOF, so that last record is not written. Add a dummy line here:
		logdata <- c(logdata, "15:00:00 end of file")  # bug report from Markus...
		
		# There may not be data (no logging until file was opened again):
		if(datastarts[i]+1 == logfilestarts[i+1])next
		
		# initialize
		iscomment <- TRUE
		lastcomment <- "no comment found"
		curleaf <- c()
		curfluorline <- NA  # Line from which fluor columns will be read (optional).
		
		# before label line, often a comment is pasted:
		maybecomment <- r[datastarts[i]-1]
		maybecomment <- strsplit(maybecomment, "\t")[[1]][1]
		
		# If maybecomment is actually 'STARTOFDATA' go back one line:
		if(length(grep("$STARTOFDATA$",maybecomment,fixed=TRUE)) > 0)maybecomment <- r[datastarts[i]-2]
		
		# Init:
		fluorcommentlines <- NA
		
		# Start at the top
		for(k in 1:length(logdata)){
			
			# Is it a comment
			iscomment_fun <- function(linenr){
				
				# In some OPEN versions, comment starts with time (no "\")
				# Time is recognized by XX:XX, that is two characters before and
				# after the ":".
				if(	nchar(strsplit(logdata[linenr], ":")[[1]][1]) == 2 &
					nchar(strsplit(logdata[linenr], ":")[[1]][2]) == 2){
						comment <- TRUE
				} else {
						comment <- FALSE
				}
				
				# In others, comment starts with "\", and then time.
				if(substr(logdata[linenr],1,1) == "\"") comment <- TRUE        
				#"
				
			return(comment)
			}
			iscomment <- iscomment_fun(k)
	
			# If we are in a fluorescence block, keep skipping until we find the comment below the fluorescence line:
			if(k %in% fluorcommentlines)next
			
            # The comment might be a Oxygen% parameter set;
            if(length(grep("Const=", logdata[k]))){
              # make sure
              if(length(grep("oxygen", tolower(logdata[k]) ))){
                 # Does not save the oxygen comment, yet.
                 oxcomment <- gsub("\"|\t", " ", lastcomment)   #"
                 next 
              }
            }
            
			# Fluorescence comments will also return TRUE, but should not be logged 
			# (and should also not prompt an averaging round as below).
			# If we are at the first fluorescence comment line, increment k appropriately
			# and find fluor parameters from log line just below:
			isfluor_fun <- function(linenr){
			
				# Normally five lines with fluorescence data.
				# Sometimes less though (a wrong entry).
				check1 <- length(grep("Fs=", logdata[linenr])) > 0
				check2 <- length(grep("Flash#", logdata[linenr+1])) > 0
				check3 <- length(grep("Fm'=", logdata[linenr+2])) > 0
				check4 <- length(grep("Dark, ", logdata[linenr+3])) > 0
				check5 <- length(grep("Fo'=", logdata[linenr+4])) > 0

			nrfluorlines <- sum(c(check1,check2,check3,check4,check5))
			return(nrfluorlines)	
			}
			
			nrfluorl <- isfluor_fun(k)
			if(nrfluorl == 0)
				isfluor <- FALSE
			else
				isfluor <- TRUE
			
			# Store where the reading with fluorescence data is, and store the line numbers of the fluorescence comments:
			if(isfluor){
				curfluorline <- k + nrfluorl
				fluorcommentlines <- k:(k+nrfluorl)  # note this includes the gas exchange line, which is logged already.
				next
			}
			
			# If it is a comment, or end of chunk is reached, write data from before
			if(iscomment | k==length(logdata)) {
				
				# First time around (no data yet).
				if(length(curleaf) == 0){
					lastcomment <- as.character(logdata[k])
                    lastcomment <- trim(lastcomment)
                    
                    # replace comma with semicolon.
                    lastcomment <- gsub(",", ";", lastcomment,fixed=TRUE)
                    lastcomment <- gsub("\"","",lastcomment)   #"
                    lastcomment <- gsub("\t"," ",lastcomment) 
                    
                    next
				}
				
				# If end of chunk is reached, include also this last line (unless it is a comment!).
				if(k==length(logdata)){
					if(!iscomment)curleaf <- c(curleaf, k)
				}
				
				# Find the date for the current chunk.
				# It always comes right after the line with "OPEN"
				DATE <- r[logfilestarts[i]+1]
				DATE <- strsplit(DATE, "\t+$")[[1]][1] # get rid of trailing tabs
				
				# Get the timestamp:
				s <- strsplit(DATE, " ")[[1]]
				TIME <- strptime(s[length(s)], "%H:%M:%S")
				dectime <- dectime_fun(TIME)
				
				# Function for reading a (vector of) text line(s) into a dataframe:
				readp <- function(x){
					p <- as.data.frame(t(do.call("cbind",strsplit(x,","))))   # comma delimited
					if(length(p) == 1)p <- as.data.frame(t(do.call("cbind",strsplit(x,"\t"))))   # tab delimited
					return(p)
				}
				
				# Read data:
				x <- logdata[curleaf]
				p <- readp(x)
				N_observations <- nrow(p)
		
				# Return either averaged data, or raw data:
				if(average)
					meandata <- as.data.frame(lapply(p,safemean))
				else
					meandata <- p
				
				
				# Read fluorescence:
				if(!all(is.na(fluorcols)) & !is.na(curfluorline)){
            
                
					fluordataline <- readp(logdata[curfluorline])[1,]
                
                    
					if(length(fluordataline) > 1){
                        meandata[,fluorcols] <- fluordataline[,fluorcols]
                    }
                }
				
				# If no last comment was found, put in 'maybecomment', and print a warning:
				if(lastcomment == "no comment found"){
					lastcomment <- maybecomment
					warning("Misplaced comment on line ",datastarts[i]-1," was used.", call.=FALSE)
				}
           
				# Vector with date, unit name, comment, and averaged data.
				predata <- data.frame(DATE,UnitName,OPENversion, lastcomment)
				meandata <- cbind(predata[rep(1,nrow(meandata)),], meandata)
				
				# add range in photosynthesis, range in stomatal conductance, range in CO2R
				f <- function(x){
					d <- as.numeric(as.character(x))
					range(d)[2] - range(d)[1]
				}

				ran1 <- f(p[,grep("Photo",labelnames)])  # photosynthesis
				ran2 <- f(p[,grep("Cond",labelnames)[1]])  # conductance
				ran3 <- f(p[,grep("CO2R",labelnames)]) # CO2R

				postdata <- data.frame(ran1, ran2, ran3, N_observations)
				meandata <- cbind(meandata, postdata[rep(1,nrow(meandata)),])			
				# WARNING: if adding more variables here, update 'varnames' at top of function.
				
				# Make as numeric
				meandata <- numericdfr(meandata)
				names(meandata) <- varnames
				
				# Recalculate variables for stomatal ratio and leaf area. 
				if(recompute){
				if(!is.na(Area))meandata$Area <- Area
				x <- meandata
				gbw <- BLCoffset + BLCslope*x$Area
				x$Trmmol <- x$Flow*(x$H2OS - x$H2OR) / (100*x$Area*(1000-x$H2OS))  # mol!
				x$Photo <- x$Flow*(x$CO2R - x$CO2S)/(100*x$Area) - x$CO2S*x$Trmmol
				eL <- 0.61365*exp((17.502*x$Tleaf)/(240.97+x$Tleaf))
				Wl <- 1000*eL/x$Press
				gtw <- x$Trmmol*(1000 - (Wl + x$H2OS)/2) / (Wl - x$H2OS)
				kf <- (K^2+1)/(K+1)^2
				x$Cond <- 1/ (1/gtw - kf/gbw)
				gtc <- 1/(1.6/x$Cond + 1.37*kf/gbw)
				x$Ci <- ((gtc - x$Trmmol/2)*x$CO2S - x$Photo) / (gtc + x$Trmmol/2)
				x$Trmmol <- 1000*x$Trmmol
				meandata <- x
				}
				
				# write to file
				if(writefile){
					write.table(meandata, outputfile, append=TRUE, 
					sep=",", col.names=FALSE, row.names=FALSE
					)
				}
				
				# Save data if to be returned:
				if(returndata){
					datalist[[elnr]] <- meandata
					elnr <- elnr + 1
				}
				# Read comment (to be written when the next comment is found!).
				lastcomment <- as.character(logdata[k])
				lastcomment <- trim(lastcomment)
                
                # replace comma with semicolon, and delete escaped ", if any.
                lastcomment <- gsub(",", ";", lastcomment)
				lastcomment <- gsub("\"","",lastcomment)   #"
                lastcomment <- gsub("\t"," ",lastcomment) 
                
				# del data
				curleaf <- c()
			}
			
			# If it is data, add the line number to 'curleaf'.
			if(!iscomment){
				curleaf <- c(curleaf, k)
			}
		}
	}

	datafr <- do.call("rbind", datalist)
	if(returndata){
		names(datafr) <- varnames
		return(datafr)
	}
return(invisible(datafr))
}
